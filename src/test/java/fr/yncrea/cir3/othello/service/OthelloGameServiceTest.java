package fr.yncrea.cir3.othello.service;

import fr.yncrea.cir3.othello.domain.othello.*;
import fr.yncrea.cir3.othello.domain.security.User;
import fr.yncrea.cir3.othello.exception.othello.OthelloCreateGameException;
import fr.yncrea.cir3.othello.exception.othello.OthelloPlayException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class OthelloGameServiceTest {
    private OthelloGameService service;
    private User white;
    private User black;

    @BeforeEach
    public void beforeEach() {
        service = new OthelloGameService();

        white = new User();
        white.setId(1L);

        black = new User();
        black.setId(2L);
    }

    private OthelloGame loadFromString(String data) {
        // create a new game
        OthelloGame game = service.create(white, black, OthelloGameSize.EIGHT);

        // clear existing pawns
        game.getPawns().clear();

        // enforce players
        game.setWhite(white);
        game.setBlack(black);

        // load pawns from string
        String[] lines = data.split("\n");
        for (int y = 0; y < lines.length; ++y) {
            String[] columns = lines[y].split(" ");
            for (int x = 0; x < columns.length; ++x) {
                switch (columns[x]) {
                    case "b":
                        game.addPawn(new OthelloPawn(x, y, OthelloPawnColor.BLACK));
                        break;

                    case "w":
                        game.addPawn(new OthelloPawn(x, y, OthelloPawnColor.WHITE));
                        break;

                    default:
                }
            }
        }

        return game;
    }

    @Test
    @DisplayName("Check that starting a game without players throws an error")
    public void testCannotStartGameWithoutPlayer() {
        assertThrows(OthelloCreateGameException.class, () -> service.create(null, null, OthelloGameSize.EIGHT));
    }

    @Test
    @DisplayName("Check that starting a game without white player throws an error")
    public void testCannotStartGameWithoutWhitePlayer() {
        assertThrows(OthelloCreateGameException.class, () -> service.create(null, black, OthelloGameSize.EIGHT));
    }

    @Test
    @DisplayName("Check that starting a game without black player throws an error")
    public void testCannotStartGameWithoutBlackPlayer() {
        assertThrows(OthelloCreateGameException.class, () -> service.create(white, null, OthelloGameSize.EIGHT));
    }

    @Test
    @DisplayName("Check that starting a game with the same player throws an error")
    public void testCreateSamePlayer() {
        assertThrows(OthelloCreateGameException.class, () -> service.create(white, white, OthelloGameSize.EIGHT));
    }

    @Test
    @DisplayName("Check the initial state of a new game")
    public void testCreateEight() {
        OthelloGame game = service.create(white, black, OthelloGameSize.EIGHT);

        // check the score (JUnit assertion)
        assertEquals(2, game.getWhiteScore());
        assertEquals(2, game.getBlackScore());

        // check the current player (AssertJ assertion)
        assertThat(game.getTurn()).isEqualTo(OthelloPawnColor.BLACK);

        // check the game status
        assertEquals(OthelloGameStatus.STARTED, game.getStatus());

        // check initial state
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(4, 4));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(3, 3));
        assertEquals(OthelloPawnColor.WHITE, game.getPawnColorAt(4, 3));
        assertEquals(OthelloPawnColor.WHITE, game.getPawnColorAt(3, 4));
    }

    @Test
    @DisplayName("Check basic capture detection")
    public void testValidCapture() {
        OthelloGame game = loadFromString("""
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -
                - - - w b - - -
                - - - b w - - -
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -""");
        service.play(game, new OthelloPawn(2, 3, OthelloPawnColor.BLACK));

        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(3, 3));
    }

    @Test
    @DisplayName("Check all directions capture detection")
    public void testValidCaptureAllDirections() {
        OthelloGame game = loadFromString("""
                - - - b - - b -
                - b - w - w - -
                - - w w w - - -
                - b w - w w b -
                - - w w w - - -
                - b - b - w - -
                - - - - - - b -
                - - - - - - - -""");

        service.play(game, new OthelloPawn(3, 3, OthelloPawnColor.BLACK));

        // assert capture
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(2, 2));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(3, 2));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(4, 2));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(2, 3));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(4, 3));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(2, 4));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(3, 4));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(4, 4));

        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(3, 1));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(5, 1));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(5, 3));
        assertEquals(OthelloPawnColor.BLACK, game.getPawnColorAt(5, 5));

        // check that no white pawn remains
        for (OthelloPawn pawn : game.getPawns()) {
            assertThat(pawn.getColor()).isEqualTo(OthelloPawnColor.BLACK);
        }
    }

    @Test
    @DisplayName("Check invalid move : no capture")
    public void testInvalidMoveNoCapture() {
        OthelloGame game = loadFromString("""
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -
                - - - w b - - -
                - - - b w - - -
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -""");
        assertThrows(OthelloPlayException.class, () -> service.play(game, new OthelloPawn(5, 3, OthelloPawnColor.BLACK)));
    }

    @Test
    @DisplayName("Check invalid move : over existing pawn")
    public void testInvalidMoveOverExistingPawn() {
        OthelloGame game = loadFromString("""
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -
                - - b w b - - -
                - - - b w - - -
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -""");
        assertThrows(OthelloPlayException.class, () -> service.play(game, new OthelloPawn(2, 3, OthelloPawnColor.BLACK)));
    }

    @Test
    @DisplayName("Check invalid move : wrong player")
    public void testInvalidWrongPlayer() {
        OthelloGame game = loadFromString("""
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -
                - - - w b - - -
                - - - b w - - -
                - - - - - - - -
                - - - - - - - -
                - - - - - - - -""");
        Exception e = assertThrows(OthelloPlayException.class, () -> service.play(game, new OthelloPawn(5, 3, OthelloPawnColor.WHITE)));

        assertEquals("C'est n'est pas votre tour de jouer", e.getMessage());
    }

    @Test
    @DisplayName("Check end game detection : full board")
    public void testEndFullBoard() {
        OthelloGame game = loadFromString("""
                w w w w w w w w
                w w w w w w w w
                w w w w w w w w
                w w w w w w w w
                b b b b b b b b
                b b b b b b b b
                b b b b b b b b
                b b b b b b w -""");

        assertEquals(OthelloGameStatus.STARTED, game.getStatus());
        service.play(game, new OthelloPawn(7, 7, OthelloPawnColor.BLACK));
        assertEquals(OthelloGameStatus.FINISHED, game.getStatus());
    }

    @Test
    @DisplayName("Check end game detection : not full board")
    public void testEndNotFullBoard() {
        OthelloGame game = loadFromString("""
                w w w w w w w w
                w w w w w w w w
                w w w w w w w w
                - - - - - - - -
                - - - - - - - -
                b b b b b b b b
                b b b b b b b b
                b b b b b b w -""");

        assertEquals(OthelloGameStatus.STARTED, game.getStatus());
        service.play(game, new OthelloPawn(7, 7, OthelloPawnColor.BLACK));
        assertEquals(OthelloGameStatus.FINISHED, game.getStatus());
    }

    @Test
    @DisplayName("Check winner detection : white")
    public void testWinnerWhite() {
        OthelloGame game = loadFromString("""
                w w w w w w w w
                w w w w w w w w
                w w w w w w w w
                w - - - - - - -
                - - - - - - - -
                b b b b b b b b
                b b b b b b b b
                b b b b b b w -""");

        assertEquals(OthelloGameStatus.STARTED, game.getStatus());
        service.play(game, new OthelloPawn(7, 7, OthelloPawnColor.BLACK));
        assertEquals(OthelloGameStatus.FINISHED, game.getStatus());
        assertEquals(white, game.getWinner());
        assertEquals(25, game.getWhiteScore());
        assertEquals(24, game.getBlackScore());
    }

    @Test
    @DisplayName("Check winner detection : black")
    public void testWinnerBlack() {
        OthelloGame game = loadFromString("""
                w w w w w w w w
                w w w w w w w w
                w w w w w w w w
                - - - - - - - -
                b - - - - - - -
                b b b b b b b b
                b b b b b b b b
                b b b b b b w -""");

        assertEquals(OthelloGameStatus.STARTED, game.getStatus());
        service.play(game, new OthelloPawn(7, 7, OthelloPawnColor.BLACK));
        assertEquals(OthelloGameStatus.FINISHED, game.getStatus());
        assertEquals(black, game.getWinner());
        assertEquals(24, game.getWhiteScore());
        assertEquals(25, game.getBlackScore());
    }

    @Test
    @DisplayName("Check winner detection : draw")
    public void testWinnerDraw() {
        OthelloGame game = loadFromString("""
                w w w w w w w w
                w w w w w w w w
                w w w w w w w w
                - - - - - - - -
                - - - - - - - -
                b b b b b b b b
                b b b b b b b b
                b b b b b b w -""");

        assertEquals(OthelloGameStatus.STARTED, game.getStatus());
        service.play(game, new OthelloPawn(7, 7, OthelloPawnColor.BLACK));
        assertEquals(OthelloGameStatus.FINISHED, game.getStatus());
        assertNull(game.getWinner());
        assertEquals(24, game.getWhiteScore());
        assertEquals(24, game.getBlackScore());
    }

    @Test
    @DisplayName("Check player cannot play")
    public void testPlayerCannotPlay() {
        OthelloGame game = loadFromString("""
                w w w w w w w w
                w w w w w w w w
                w w w w w w w w
                - - - - - - - -
                - - - - - - - -
                b w - b b b b b
                b b b b b b b b
                b b b b b b w -""");

        assertEquals(OthelloGameStatus.STARTED, game.getStatus());
        assertEquals(OthelloPawnColor.BLACK, game.getTurn());
        service.play(game, new OthelloPawn(7, 7, OthelloPawnColor.BLACK));
        assertEquals(OthelloPawnColor.BLACK, game.getTurn());
    }
}