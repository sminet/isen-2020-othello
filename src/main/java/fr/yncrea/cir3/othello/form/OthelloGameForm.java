package fr.yncrea.cir3.othello.form;

import fr.yncrea.cir3.othello.domain.othello.OthelloGameSize;
import fr.yncrea.cir3.othello.service.constraints.StringEnumeration;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OthelloGameForm {
    @NotNull
    private Long player;

    @StringEnumeration(enumClass = OthelloGameSize.class, message = "Taille invalide")
    private String size;
}
