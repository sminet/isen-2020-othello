package fr.yncrea.cir3.othello.controller;

import fr.yncrea.cir3.othello.domain.security.User;
import fr.yncrea.cir3.othello.repository.OthelloGameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @Autowired
    private OthelloGameRepository games;

    @GetMapping({"/", ""})
    public String index(Model model, @PageableDefault(page = 0, size = 10) Pageable pageable, @AuthenticationPrincipal User user) {
        model.addAttribute("games", games.findByWhiteOrBlack(user, pageable));

        return "index/index";
    }

    @GetMapping("/user-info")
    public String userInfo() {
        return "index/user-info";
    }
}
