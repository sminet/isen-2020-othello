package fr.yncrea.cir3.othello.controller;

import fr.yncrea.cir3.othello.domain.message.FlashMessage;
import fr.yncrea.cir3.othello.domain.message.FlashMessageLevel;
import fr.yncrea.cir3.othello.domain.othello.OthelloGame;
import fr.yncrea.cir3.othello.domain.othello.OthelloGameSize;
import fr.yncrea.cir3.othello.domain.othello.OthelloPawn;
import fr.yncrea.cir3.othello.domain.security.User;
import fr.yncrea.cir3.othello.exception.othello.OthelloCreateGameException;
import fr.yncrea.cir3.othello.exception.othello.OthelloPlayException;
import fr.yncrea.cir3.othello.form.OthelloGameForm;
import fr.yncrea.cir3.othello.repository.OthelloGameRepository;
import fr.yncrea.cir3.othello.repository.UserRepository;
import fr.yncrea.cir3.othello.service.OthelloGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/othello")
public class OthelloController {
    @Autowired
    private UserRepository users;

    @Autowired
    private OthelloGameRepository games;

    @Autowired
    private OthelloGameService service;

    @GetMapping("/start")
    public String startForm(Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("game", new OthelloGameForm());
        model.addAttribute("sizes", OthelloGameSize.values());
        model.addAttribute("users", users.findByIdNot(user.getId(), Sort.by("username")));

        return "othello/start";
    }

    @PostMapping("/start")
    public String start(@Valid @ModelAttribute("game") OthelloGameForm form, BindingResult result, Model model, @AuthenticationPrincipal User user) {
        try {
            if (result.hasErrors()) {
                throw new OthelloCreateGameException("Impossible de créer la partie");
            }

            User opponent = users.findById(form.getPlayer()).orElse(null);
            OthelloGameSize size = OthelloGameSize.valueOf(form.getSize());

            OthelloGame game = service.create(user, opponent, size);
            games.save(game);

            return "redirect:/othello/play/" + game.getId();
        } catch (OthelloCreateGameException e) {
            model.addAttribute("game", form);
            model.addAttribute("sizes", OthelloGameSize.values());
            model.addAttribute("users", users.findAll(Sort.by("username")));
            model.addAttribute("message", new FlashMessage(e.getMessage(), FlashMessageLevel.ERROR));

            return "othello/start";
        }
    }

    @GetMapping("/play/{id}")
    public String create(@PathVariable Long id, Model model) {
        model.addAttribute("game", games.findById(id).orElseThrow(() -> new RuntimeException("Not found")));

        return "othello/play";
    }

    @GetMapping("/play/{id}/{x}/{y}")
    public String play(@PathVariable Long id, @PathVariable int x, @PathVariable int y, RedirectAttributes attributes, @AuthenticationPrincipal User user) {
        OthelloGame game = games.findById(id).orElseThrow(() -> new RuntimeException("Not found"));

        try {
            service.play(game, new OthelloPawn(x, y, game.getPlayerColor(user)));
            games.save(game);

        } catch (OthelloPlayException e) {
            attributes.addFlashAttribute("message", new FlashMessage(e.getMessage(), FlashMessageLevel.ERROR));
        }

        return "redirect:/othello/play/" + id;
    }

    @GetMapping("/delete/{id}")
    public String delete(HttpServletRequest request, @PathVariable Long id) {
        games.deleteById(id);

        String redirect = "/";
        String referer = request.getHeader("Referer");
        if (referer != null) {
            redirect = referer;
        }

        return "redirect:" + referer;
    }
}
