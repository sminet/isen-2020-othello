package fr.yncrea.cir3.othello.domain.message;

public enum FlashMessageLevel {
    ERROR("danger"), WARNING("warning"), INFO("info"), SUCCESS("success");

    private String name;

    private FlashMessageLevel(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
