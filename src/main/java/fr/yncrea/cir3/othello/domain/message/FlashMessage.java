package fr.yncrea.cir3.othello.domain.message;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class FlashMessage implements Serializable {
    private String text;
    private FlashMessageLevel level;
}
