package fr.yncrea.cir3.othello.domain.othello;

import fr.yncrea.cir3.othello.domain.security.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "othello_game")
public class OthelloGame {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "othello_game")
    @SequenceGenerator(name="othello_game", sequenceName = "seq_othello_game")
    private Long id;

    private LocalDateTime created = LocalDateTime.now();

    @Enumerated(EnumType.STRING)
    private OthelloGameSize size;

    @ManyToOne(optional = false)
    private User white;

    @ManyToOne(optional = false)
    private User black;

    @Enumerated(EnumType.STRING)
    private OthelloPawnColor turn = OthelloPawnColor.BLACK;

    @Enumerated(EnumType.STRING)
    private OthelloGameStatus status = OthelloGameStatus.STARTED;

    private int whiteScore;
    private int blackScore;

    @ManyToOne
    private User winner;

    @OrderColumn
    @ElementCollection
    @CollectionTable(name = "othello_game_pawn")
    private List<OthelloPawn> pawns;

    public void addPawn(OthelloPawn pawn) {
        if (pawns == null) {
            pawns = new ArrayList<>();
        }

        pawns.add(pawn);
    }

    public OthelloPawn getPawnAt(int x, int y) {
        if (pawns == null) {
            return null;
        }

        for (OthelloPawn pawn : pawns) {
            if (pawn.getX() == x && pawn.getY() == y) {
                return pawn;
            }
        }

        return null;
    }

    public OthelloPawnColor getPawnColorAt(int x, int y) {
        OthelloPawn pawn = getPawnAt(x, y);
        if (pawn == null) {
            return null;
        }

        return pawn.getColor();
    }

    public OthelloPawnColor getPlayerColor(User user) {
        if (Objects.equals(user.getId(), getWhite().getId())) {
            return OthelloPawnColor.WHITE;
        } else if (Objects.equals(user.getId(), getBlack().getId())) {
            return OthelloPawnColor.BLACK;
        }

        return null;
    }

    public void updateScore() {
        whiteScore = (int) pawns.stream().filter(e -> e.getColor() == OthelloPawnColor.WHITE).count();
        blackScore = (int) pawns.stream().filter(e -> e.getColor() == OthelloPawnColor.BLACK).count();
    }
}
