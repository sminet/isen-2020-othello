package fr.yncrea.cir3.othello.repository;

import fr.yncrea.cir3.othello.domain.othello.OthelloGame;
import fr.yncrea.cir3.othello.domain.security.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OthelloGameRepository extends JpaRepository<OthelloGame, Long> {
    Page<OthelloGame> findByWhiteOrBlack(User white, User black, Pageable pageable);

    default Page<OthelloGame> findByWhiteOrBlack(User user, Pageable pageable) {
        return findByWhiteOrBlack(user, user, pageable);
    }
}
